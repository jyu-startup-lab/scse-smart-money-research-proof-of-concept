package com.template.states

import com.template.contracts.SmartMoneyContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.ContractState
import net.corda.core.identity.AbstractParty

// *********
// * Smart money state *
// *********
@BelongsToContract(SmartMoneyContract::class)
data class SmartMoneyState(val data: String, override val participants: List<AbstractParty> = listOf()) : ContractState
