package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.api.flows.account.CreateAccountFlow

import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.messaging.startFlow
import net.corda.core.utilities.getOrThrow
import java.util.*

// *********
// * Account-related flows *
// *********

/**
 * Creates a new account on a node.
 * @property rpcOps CordaRPCOps
 * @constructor
 */
@InitiatingFlow
@StartableByRPC
class CreateAccount(private val rpcOps: CordaRPCOps/*, private val accountId: String*/): FlowLogic<String>() {

    @Suspendable
    override fun call(): String {
        // Obtain a reference to a wanted notary.
        val notary = serviceHub.networkMapCache.notaryIdentities.first()

        val newAccountName = UUID.randomUUID().toString()

        val accountRequests = listOf(CreateAccountFlow.Request(newAccountName))
        rpcOps.startFlow(::CreateAccountFlow, accountRequests, notary).returnValue.getOrThrow()

        return newAccountName
    }
}

/**
 * Returns token balance for the wanted account ID.
 * @property accountIdCriteria
 * @constructor
 */
/*@InitiatingFlow
@StartableByRPC
class BalanceForAccount(accountId: String): Set<Amount<TokenType.Descriptor>> {
    val accountIdCriteria = VaultCustomQueryCriteria(PersistedToken::accountId.equal(accountId))
    //return
}*/